FROM gradle:jdk8 as build
WORKDIR /app
COPY /source /app
RUN gradle build

FROM openjdk:8-alpine
COPY --from=build /app/build/libs/tchallenge-service.jar tchallenge-service.jar
EXPOSE 4567
ENTRYPOINT ["java", "-jar", "tchallenge-service.jar"]
